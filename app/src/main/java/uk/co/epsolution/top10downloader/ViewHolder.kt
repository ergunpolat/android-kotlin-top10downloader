package uk.co.epsolution.top10downloader

import android.view.View
import android.widget.TextView

/**
 * Created by Ergun Polat on 13/01/2018.
 */
class ViewHolder(v: View) {
    val tvName: TextView = v.findViewById(R.id.tvName)
    val tvArtist: TextView = v.findViewById(R.id.tvArtist)
    val tvSummary: TextView = v.findViewById(R.id.tvSummary)
}